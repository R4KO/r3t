#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include "../include/fonctions.h"
#include "../include/questions.h"

void CreateText(SDL_Surface *screen, int x, int y,char* ex,SDL_Color fontColor, TTF_Font* fontTest)
{
    SDL_Surface *texte1 = NULL;
    texte1 = TTF_RenderText_Solid(fontTest,ex,fontColor);

    SDL_Rect texte1Position;


    texte1Position.x = x;
    texte1Position.y = y;

    SDL_BlitSurface(texte1,NULL,screen,&texte1Position);
}




