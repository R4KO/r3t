#include <stdlib.h>
#include <stdio.h>

#include "SDL/SDL.h"

#include "../include/timer.h"

Timer* init_Timer() {
    Timer* myTimer = NULL;
    myTimer = malloc(sizeof(Timer));

    if (myTimer == NULL) {
        printf("Can not initialize the Timer\n");
        exit(EXIT_FAILURE);
    }

    // Default value of our instance
    myTimer->startTicks = 0;
    myTimer->started = false;

    return myTimer;
}

void clean_Timer(Timer** myTimer) {
    free(*myTimer);
    *myTimer = NULL;
}

void start_Timer(Timer* myTimer) {
    myTimer->started = true;
    myTimer->startTicks = SDL_GetTicks();
}

void stop_Timer(Timer* myTimer) {
    myTimer->started = false;
}

int getTicks_Timer(Timer* myTimer) {
    // Is our Timer running ?
    if (isStarted_Timer(myTimer)) {
            // We return the ticks since the start
            return SDL_GetTicks() - myTimer->startTicks;
    }

    return 0;
}

bool isStarted_Timer(Timer* myTimer) {
    // Return the status of our Timer
    return myTimer->started;
}

float getTime_Timer(Timer* myTimer) {
    // Return time that passed since the init of the timer in seconds
    return (float) getTicks_Timer(myTimer) / 1000;
}

