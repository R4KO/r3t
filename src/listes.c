//
// Created by thoma on 17/11/2018.
//
#include <stdio.h>
#include <stdlib.h>
#include "../include/listes.h"
#include "../include/questions.h"

liste new_node(struct node* ex){
    liste l = malloc(sizeof(node_list));
    l->questions = ex;
    l->next = NULL;
    return l;
};

liste init_list(FILE *sauv){
    if(feof(sauv)) return NULL;
    liste l = malloc(sizeof(node_list));
    l->questions = malloc(sizeof(node));
    fill_question(l->questions,sauv);
    l->next = init_list(sauv);
    return l;
}

void print_list_rec(liste l) {
    if (!l) printf("\n---------------------FIN---------------------\n");
    else {
        disp_question(*(l->questions));
        print_list_rec(l->next);
    }
}

void print_1_list(liste l, unsigned n){
    if(!l) printf("Empty list\n");
    else if(((n-1) != 0)&&(l->next))
        print_1_list(l->next, n-1);
    else if(n-1 == 0) disp_question(*(l->questions));
    else printf("Out of range\n");
}

node* return_1_list(liste l, unsigned n){
    if(!l) {
        printf("Empty list\n");
        return NULL;
    }
    else if(((n-1) != 0)&&(l->next)) {
        return return_1_list(l->next, n - 1);
    }
    else if(n-1 == 0) return l->questions;
    else {
        printf("Out of list range\n");
        return NULL;
    }
}

void free_list(liste* l){
    if(*l) {
        free_list(&(*l)->next);
        free_question(&((*l)->questions));
        (*l)->questions = NULL;
        free(*l);
        *l = NULL;
    }
}

/*void print_question(liste l, unsigned n){
    if(!l) printf("Empty list\n");
    else if(((n-1) != 0)&&(l->next))
        print_1_list(l->next, n-1);
    else if(n-1 == 0) ask_question(*(l->questions));
    else printf("Out of range\n");
}*/

int length(liste l){
    if(!l) return 0;
    return 1 + length(l->next);
}

int max(int a, int b){
    return a >= b ? a : b;
}