//
// Created by thoma on 08/12/2018.
//

#include <stdbool.h>
#include "../include/my_events.h"
#include "../include/listes.h"
#include "../include/jeu.h"

bool check_coords(Sint16 a, Uint16 b, Uint16 c){
    return (a <= c) && (a+b >= c) ? true : false;
}

bool check_Button(SDL_Rect button, SDL_MouseButtonEvent event){
    return check_coords(button.x, button.w, event.x) && check_coords(button.y, button.h, event.y) ? true : false;
}

int check_every_button(SDL_Rect* button, SDL_MouseButtonEvent event){
    if(check_Button(*button, event)){
        //printf("Bouton 1\n");
        return 1;
    }else if(check_Button(*(button + 1 ), event)){
        //printf("Bouton 2\n");
        return 2;
    } else if(check_Button(*(button + 2), event)){
        //printf("Bouton 3\n");
        return 3;
    }else if(check_Button(*(button + 3), event)){
        //printf("Bouton 4\n");
        return 4;
    }else{
        return 0;
    }
}

void mousePress(SDL_MouseButtonEvent event, int* c, SDL_Rect* rect, tree_node** l, int* lock){
    if (event.button == SDL_BUTTON_LEFT && *c){
        // check buttons
        int bouton_choisi = check_every_button(rect, event);
        if(bouton_choisi) {
            printf("Niveau actuel : %d\n", (*l)->info->lvl);
            // send answer to jeu.c
            int a = get_answer(bouton_choisi, (*l)->info);
            int dir = a == 0;
            printf("Niveau maximum à atteindre : %d\n", maxABR(*l));
            if(!a){
                printf("Question : %s\n",(*l)->info->question);
                printf("Bonne réponse\n");
                printf("Réponse : %s\n\n", (*l)->info->answer);
            }else{
                printf("Question : %s\n",(*l)->info->question);
                printf("Mauvaise réponse\n");
                printf("La bonne réponse était : %s\n\n",(*l)->info->answer);
            }

            free_tree_node_recursive(&((*l)->sa[!dir]));
            *l = (*l)->sa[dir];

            useful_print_tree(*l, 0);

            printf("\n");
        }
        *c = 0;
    }
}
