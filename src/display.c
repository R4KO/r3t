//
// Created by thoma on 03/12/2018.
//

#include "../include/my_events.h"
#include "../include/display.h"
#include "../include/listes.h"
#include "../include/tree.h"
#include "../include/fonctions.h"
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

#define LARGEUR 1000
#define HAUTEUR 666
#define bpp 32
#define SIZE 20

void display(tree_node* l){
    SDL_Surface *screen = NULL;
    SDL_Surface* buttons[4];
    SDL_Rect rect[4];

    SDL_Init(SDL_INIT_VIDEO);

    screen = SDL_SetVideoMode(LARGEUR, HAUTEUR, bpp, SDL_SWSURFACE);
    SDL_WM_SetCaption("Road To The Top", NULL);

    TTF_Init();
    TTF_Font* fontTest;
    SDL_Color fontColor;

    init_buttons(rect, buttons, &fontTest, &fontColor);

    /// Afichage de la fenêtre et récupération des évènemets
    event_collector(screen, rect, buttons, fontTest, fontColor, l);

    for (int i = 0; i < 4; ++i) {
        SDL_FreeSurface(buttons[i]);
    }
    SDL_Quit();
    TTF_Quit();
}

void disp_question_on_screen(SDL_Surface* screen, node* ex, SDL_Color fontColor, TTF_Font* f){
    CreateText(screen, 0, 0, ex->question, fontColor, f);
    CreateText(screen, 0, 50, ex->prop_1, fontColor, f);
    CreateText(screen, 0, 75, ex->prop_2, fontColor, f);
    CreateText(screen, 0, 100, ex->prop_3, fontColor, f);
    CreateText(screen, 0, 125, ex->prop_4, fontColor, f);

}

void init_buttons(SDL_Rect* rect, SDL_Surface* *buttons, TTF_Font** f, SDL_Color *fc){
    buttons[0] = IMG_Load("../assets/images/button_A.png");
    buttons[1] = IMG_Load("../assets/images/button_B.png");
    buttons[2] = IMG_Load("../assets/images/button_C.png");
    buttons[3] = IMG_Load("../assets/images/button_D.png");

    if(!(buttons[0]&&buttons[1]&&buttons[2]&&buttons[3])){
        printf("IMG_Load: %s\n", IMG_GetError());
        exit(EXIT_FAILURE);
    }

    // bouton haut gauche
    rect->x = 0;
    rect->y = 334;
    rect->w = 200;
    rect->h = 166;

    // bouton haut droite
    (rect+1)->x = 200;
    (rect+1)->y = rect->y;
    (rect+1)->w = rect->w;
    (rect+1)->h = rect->h;

    // bouton bas gauche
    (rect+2)->x = rect->x;
    (rect+2)->y = rect->y + rect->h;
    (rect+2)->w = rect->w;
    (rect+2)->h = rect->h;

    // bouton bas droite
    (rect+3)->x = (rect+2)->x + rect->w;
    (rect+3)->y = (rect+2)->y;
    (rect+3)->w = rect->w;
    (rect+3)->h = rect->h;

    // ouverture de la font pour les questions
    //*f = TTF_OpenFont("../assets/fonts/abhaya-libre/AbhayaLibre-Regular.ttf", SIZE);
    *f = TTF_OpenFont("../assets/fonts/test(2).ttf", SIZE);
    fc->r = 0;
    fc->g = 0;
    fc->b = 0;
}

void event_collector(SDL_Surface* screen, SDL_Rect* rect, SDL_Surface** buttons, TTF_Font* fontTest, SDL_Color fontColor, tree_node* l){
    Uint32 color = 0;
    int now = 0;
    int ex = 0;
    int FPS = 20;
    int dt = 0;
    int running = 1; // test the running
    int clock = 1; // test the click

    SDL_Event event = {0};

    /// boucle principale
    while(running){
        color = SDL_MapRGB(screen->format, 255, 128, 192);

        now = SDL_GetTicks();
        dt = now - ex;
        if(dt > FPS) { // test pour le maintien des FPS constants

            SDL_PollEvent(&event);
            switch (event.type) {
                case SDL_QUIT: { // bouton croix
                    running = 0;
                    break;
                }
                case SDL_KEYDOWN: {
                    if (event.key.keysym.sym == SDLK_ESCAPE) {
                        running = 0;
                    }
                    break;
                }
                case SDL_KEYUP: {
                    break;
                }
                case SDL_MOUSEBUTTONDOWN: {
                    if (clock) {
                        mousePress(event.button, &clock, rect, &l, &running);
                    }
                    break;
                }
                case SDL_MOUSEBUTTONUP:{
                    clock = 1; // pour lui demander de recapturer de nouveau les clics
                    break;
                }
                default: {
                    break;
                }
            }

            SDL_FillRect(screen, NULL, color);
            for(int i = 0; i < 4; i++){
                SDL_BlitSurface(buttons[i], NULL, screen, (rect+i));
            }
            // TODO: print question
            if(l) {
                disp_question_on_screen(screen, l->info, fontColor, fontTest);
            }else{
                running = 0;
            }
            SDL_Flip(screen);
            ex = now;
        }else{
            SDL_Delay(FPS - dt);
        }
    }
}
