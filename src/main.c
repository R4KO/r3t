#include <stdio.h>
#include <stdlib.h>
#include "../include/questions.h"
#include "../include/listes.h"
#include "../include/jeu.h"
#include "../include/display.h"
#include "../include/tree.h"
#include "../include/timer.h"


int main(int argc, char *argv[]){

    /// Le timer était au départ prévu pour limiter le temps de réponse aux questions. L'idée a été abandonnée ensuite.
    /*
    Timer* timer = init_Timer();

    start_Timer(timer);

    float compteur;
    do{
        compteur = getTime_Timer(timer);
        printf("%.2f\n", 10.0 - (compteur));
    }while(10.0 - compteur > 0.0);

    clean_Timer(&timer);
    */

    //setenv("DISPLAY", "127.0.0.1:0", 1);

    /// On choisit le fichier à ouvrir
    FILE *sauv;
    init_file(&sauv, *argv[1]); // 2 pour Maths, 3 pour Physique et 4 pour Sport
    //init_file(&sauv,'4');


    /// On remplit la liste chainée des questions contenues dans le fichier correspondant
    liste l = init_list(sauv);

    fclose(sauv);

    /// Ensuite on remplit l'arbre binaire de recherche avec les questions comprises dans la liste chainée
    tree* t = init_tree();

    liste l_prime = l;
    while(l_prime){
        insert_tree(t, l_prime->questions);
        l_prime = l_prime->next;
        //print_tree(*t);
    }
    print_tree(*t);

    int max = maxABR(t->root);

    display(t->root);


    printf("\nNiveau maximum absolu : %d\n", max);


    /// libération mémoire
    free_tree_recursive(&t);

    free_list(&l);

    return 0;
}
