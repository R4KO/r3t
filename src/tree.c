//
// Created by thoma on 20/11/2018.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include "../include/tree.h"
#include "../include/questions.h"
#include "../include/listes.h"

tree* init_tree(){
    tree* t = NULL;
    t = malloc(sizeof(tree));

    if(!t){
        printf("Can not initialize the tree\n");
        exit(EXIT_FAILURE);
    }
    t->root = NULL;

    return t;
}

void free_tree_recursive(tree** t){
    tree_node* tmp = (*t)->root;
    free_tree_node_recursive(&tmp);
    free(*t);
    *t = NULL;
}

void free_tree_node_recursive(tree_node** t){
    if(*t){
        free_tree_node_recursive(&((*t)->sa[0]));
        free_tree_node_recursive(&((*t)->sa[1]));
        free(*t);
        *t = NULL;
    }
}

tree_node* new_tree_node(node *ex){
    tree_node* t = malloc(sizeof(tree_node));

    if(!t){
        printf("Can not create a tree node\n");
        exit(EXIT_FAILURE);
    }

    t->info = ex;
    t->red = true;
    t->sa[0] = NULL;
    t->sa[1] = NULL;

    return t;
}

void print_prefix(char c, int n){
    while (n--) putchar(c);
}

void print_tree(tree t){
    useful_print_tree(t.root,0);
}

void useful_print_tree(tree_node* t, int p){
    print_prefix('-', 2*p);
    if(!t){
        printf("x\n");
    }else{
        printf("%d ",t->info->lvl); // on affiche la key
        if(t->red){
            printf("rouge");
        } else{
            printf("noir");
        }
        printf("\n");
        if(t->sa[0] || t->sa[1]){
            useful_print_tree(t->sa[0], p+1);
            useful_print_tree(t->sa[1], p+1);
        }
    }
}

void insert_tree(tree* t, node* ex){
    t->root = insertRecursively(t->root,ex);
    t->root->red = false;
}

tree_node* insertRecursively(tree_node* root, node* ex){
    if(!root){
        root = new_tree_node(ex);
    }else if(compare_for_direction(root->info, ex)){
        int direction = compare_for_direction(root->info, ex) < 0;
        root->sa[direction] = insertRecursively(root->sa[direction], ex);

        if(isRed(root->sa[direction])){
            if(isRed(root->sa[!direction])){
                root->red = true;
                root->sa[0]->red = false;
                root->sa[1]->red = false;
            } else{
                if(isRed(root->sa[direction]->sa[direction])){
                    root = singleRotation(root, !direction);
                } else if(isRed(root->sa[direction]->sa[!direction])){
                    root = doubleRotation(root, !direction);
                }
            }
        }
    }

    return root;
}

int compare_for_direction(node* ex, node* ex2){
    if(ex->lvl == ex2->lvl){
        return 0;
    }else if(ex->lvl < ex2->lvl){
        return -1;
    } else{
        return 1;
    }
}

bool isRed(tree_node* root){
    return root != NULL && root->red == true;
}

tree_node* singleRotation(tree_node* root, int dir){

    tree_node* save = root->sa[!dir];

    //printf("\n\n\n");

    root->sa[!dir] = save->sa[dir];
    save->sa[dir] = root;

    root->red = true;
    save->red = false;

    return save;
}

tree_node* doubleRotation(tree_node* root, int dir){
    root->sa[!dir] = singleRotation(root->sa[!dir], !dir);

    return singleRotation(root, dir);
}

int maxABR(tree_node* t){
    if(t == NULL) return -1;

    if(t->sa[1]) return maxABR(t->sa[1]);

    return t->info->lvl;
}