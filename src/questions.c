#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/questions.h"

#define BUFFER_SIZE 256

void init_file(FILE** sauv, char file){

    if(file == '1'){
        *sauv = fopen("../assets/files/sauvegarde.DAT","r"); // fichier test
    }else if(file == '2'){
        *sauv = fopen("../assets/files/maths.DAT","r");
    }else if(file == '3'){
        *sauv = fopen("../assets/files/physique.DAT","r");
    }else if(file == '4'){
        *sauv = fopen("../assets/files/sport.DAT","r");
    }else{
        printf("That file doesn't exist\n");
        exit(1);
    }

	if(!(*sauv)){
		fprintf(stderr, "Users file opening error\n");
		exit(1);
	}
}

void disp_file(FILE* sauv){

	char buffer[BUFFER_SIZE];
	char *input;

	while (!feof(sauv)){
    	fgets(buffer,BUFFER_SIZE,sauv);
        buffer[strcspn(buffer, "\r\n")] = 0;
        input = malloc((strlen(buffer)+1)*sizeof(char));
        if(!input){
            printf("Error reading file\n");
            exit(EXIT_FAILURE);
        }
        strcpy(input,buffer);
	    printf("%s [%d]\n",input, (int)strlen(input));
	    free(input);
	}
	/*while(fscanf(sauv, "%s", str) != EOF){
		printf("%s\n", str);
	}*/
}

void disp_question(node ex){
    printf("%d\n%s%s%s%s%s%s",ex.lvl,ex.question,ex.prop_1,ex.prop_2,ex.prop_3,ex.prop_4,ex.answer);
}

void fill_question(node *ex, FILE* sauv){
    fill_lvl(sauv, &(ex->lvl));
    fill_field(sauv, &(ex->question));
    fill_field(sauv, &(ex->prop_1));
    fill_field(sauv, &(ex->prop_2));
    fill_field(sauv, &(ex->prop_3));
    fill_field(sauv, &(ex->prop_4));
    fill_field(sauv, &(ex->answer));
}

void fill_field(FILE* sauv, char** ex){
    char buffer[BUFFER_SIZE];
    fgets(buffer,BUFFER_SIZE,sauv);
    buffer[strcspn(buffer, "\r\n")] = 0; // afin de retirer la caratère de retour à la ligne
    *ex = malloc((strlen(buffer)+1)*sizeof(char));
    strcpy(*ex, buffer);
}

void fill_lvl(FILE* sauv, int* level){
    fscanf(sauv,"%d\n",level);
}

void free_question(node** ex){
    free((*ex)->question);
    (*ex)->question = NULL;
    free((*ex)->prop_1);
    (*ex)->prop_1 = NULL;
    free((*ex)->prop_2);
    (*ex)->prop_2 = NULL;
    free((*ex)->prop_2);
    (*ex)->prop_1 = NULL;
    free((*ex)->prop_3);
    (*ex)->prop_1 = NULL;
    free((*ex)->prop_4);
    (*ex)->prop_4 = NULL;
    free((*ex)->answer);
    (*ex)->answer = NULL;
    free(*ex);
    *ex = NULL;
}
