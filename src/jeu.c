//
// Created by thoma on 20/11/2018.
//

#include <stdio.h>
#include <string.h>
#include "../include/jeu.h"
#include "../include/listes.h"
#include "../include/questions.h"

void jeu(liste l){
    int i = 0;
    int index = length(l)/2;
    node* l_prime = return_1_list(l, index);
    while(l){
        printf("LVL = %d\n", l_prime->lvl);
        int a = ask_question(l_prime);
        if(!a){
            printf("Bonne réponse\n");
            i++;
            l_prime = return_1_list(l,index + 1);
        }else{
            printf("Mauvaise réponse\n");
            l_prime = return_1_list(l, index - 1);
        }
        //printf("%d",a);
        l = l->next;
    }
    printf("\nPoints : %d\n",i);
}

int ask_question(node* ex){
    int answer = 0;
    printf("\nQuestion : %s\n",ex->question);
    printf("Proposition 1 : %s",ex->prop_1);
    printf("Proposition 2 : %s",ex->prop_2);
    printf("Proposition 3 : %s",ex->prop_3);
    printf("Proposition 4 : %s",ex->prop_4);
    printf("\nRéponse ? ");
    scanf("%d",&answer);
    return get_answer(answer, ex);
}

int get_answer(int a, node* ex){
    switch(a){
        case 1:{
            return strcmp(ex->prop_1, ex->answer);
            break;
        }
        case 2:{
            return strcmp(ex->prop_2, ex->answer);
            break;
        }
        case 3:{
            return strcmp(ex->prop_3, ex->answer);
            break;
        }
        case 4:{
            return strcmp(ex->prop_4, ex->answer);
            break;
        }
        default:
            break;
    }
    return -1;
}