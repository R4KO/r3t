#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED

#include <SDL/SDL_ttf.h>
#include "questions.h"

void CreateText(SDL_Surface *screen, int x, int y, char* ex, SDL_Color fontColor, TTF_Font* fontTest);

#endif // FONCTIONS_H_INCLUDED

