//
// Created by thoma on 20/11/2018.
//

#ifndef PROJET_C_TREE_H
#define PROJET_C_TREE_H

#include "listes.h"
#include <stdbool.h>

typedef struct tree_node{
    struct node* info;
    bool red;
    struct tree_node* sa[2];
}tree_node;

typedef struct tree{
    tree_node* root;
}tree;

// création d'un tree_node
tree* init_tree();

void insert_tree(tree* t, node* ex);

tree_node* insertRecursively(tree_node* root, node* ex);

int compare_for_direction(node* ex, node* ex2);

tree_node* new_tree_node(struct node *ex);

void print_tree(tree t);

void useful_print_tree(tree_node* t, int p);

void free_tree_recursive(tree** t);

void free_tree_node_recursive(tree_node** t);

bool isRed(tree_node* root);

tree_node* singleRotation(tree_node* root, int dir);

tree_node* doubleRotation(tree_node* root, int dir);

int maxABR(tree_node* t);

#endif //PROJET_C_TREE_H
