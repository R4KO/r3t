//
// Created by thoma on 20/11/2018.
//

#ifndef PROJET_C_JEU_H
#define PROJET_C_JEU_H

#include "questions.h"
#include "listes.h"

void jeu(liste l);

int ask_question(node* ex);

int get_answer(int a, node* ex);

#endif //PROJET_C_JEU_H
