//
// Created by thoma on 03/12/2018.
//

#ifndef PROJET_C_DISPLAY_H
#define PROJET_C_DISPLAY_H

#include <SDL/SDL_ttf.h>
#include "tree.h"

void display(tree_node* l);

void disp_question_on_screen(SDL_Surface* screen, node* ex, SDL_Color fontColor, TTF_Font* f);

void init_buttons(SDL_Rect* rect, SDL_Surface* *buttons, TTF_Font** f, SDL_Color* fc);

void event_collector(SDL_Surface* screen, SDL_Rect* rect, SDL_Surface** buttons, TTF_Font* fontTest, SDL_Color fontColor, tree_node* l);

#endif //PROJET_C_DISPLAY_H
