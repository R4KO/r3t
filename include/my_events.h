//
// Created by thoma on 08/12/2018.
//

#ifndef PROJET_C_MY_EVENTS_H
#define PROJET_C_MY_EVENTS_H

#include <stdbool.h>
#include <SDL/SDL_events.h>
#include "listes.h"
#include "tree.h"

void mousePress(SDL_MouseButtonEvent event, int* c, SDL_Rect* rect, tree_node** l, int* lock);

bool check_coords(Sint16 a, Uint16 b, Uint16 c);

bool check_Button(SDL_Rect button, SDL_MouseButtonEvent event);

int check_every_button(SDL_Rect* button, SDL_MouseButtonEvent event);

#endif //PROJET_C_MY_EVENTS_H
