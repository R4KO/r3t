#ifndef QUESTIONS_H_INCLUDED
#define QUESTIONS_H_INCLUDED

typedef struct node{
	int lvl;
	char *question;
	char *prop_1;
	char *prop_2;
	char *prop_3;
	char *prop_4;
	char *answer;
}node;

//typedef node *questions;

void init_file(FILE** sauv, char file);

void disp_file(FILE* sauv);

void fill_lvl(FILE* sauv, int* level);

void fill_field(FILE* sauv, char** ex);

void fill_question(node *ex, FILE* sauv);

void disp_question(node ex);

void free_question(node** ex);

#endif // QUESTIONS_H_INCLUDED