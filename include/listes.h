//
// Created by thoma on 17/11/2018.
//

#ifndef PROJET_C_LISTES_H
#define PROJET_C_LISTES_H

#include "questions.h"

typedef struct list{
    struct node* questions;
    struct list *next;
}node_list;


typedef node_list* liste;

/*
typedef struct tmp_struct{
    char* question_posee;
    char* reponse_donnee;
    char* reponse;
}struct_answer;

typedef struct list_reponses{
    struct_answer info;
    struct list_reponses *next;
}reponses_donnees;

typedef struct reponses_donnees* liste_reponses;
*/

liste new_node(struct node* ex);

void print_list_rec(liste l);

liste init_list(FILE* sauv);

void print_1_list(liste l, unsigned n);

int length(liste l);

void print_question(liste l, unsigned n);

node* return_1_list(liste l, unsigned n);

void free_list(liste* l);

#endif //PROJET_C_LISTES_H
