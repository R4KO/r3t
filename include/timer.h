#ifndef PROJET_C_ENGINE_TIMER_H
#define PROJET_C_ENGINE_TIMER_H

#include <stdbool.h>

typedef struct Timer {
    int startTicks;
    bool started;
} Timer;

Timer* init_Timer();
void clean_Timer(Timer** myTimer);

void start_Timer(Timer* myTimer);
void stop_Timer(Timer* myTimer);

int getTicks_Timer(Timer* myTimer);

float getTime_Timer(Timer* myTimer);

bool isStarted_Timer(Timer* myTimer);

#endif //PROJET_C_ENGINE_TIMER_H
