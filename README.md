# Road to the TOP

Projet de programmation du premier semestre de L2

## Tout d'abord

Ces instructions vous permettent de construire le projet sous un environnement Linux avec un invité de commandes.

### Bibliothèques nécessaires

Avant de construire le projet certaines bibliothèques sont nécessaires :

Tout d'abord SDL pour l'interface graphique.

```
sudo apt-get install libsdl1.2-dev
```

Ensuite SDL_image pour l'utilisation des images dans le dossier projet.

```
sudo apt-get install libsdl-image1.2 libsdl-image1.2-dev
```

Et enfin TTF pour la police d'écriture.

```
sudo apt install libsdl-ttf2.0-0 libsdl-ttf2.0-dev
```

### Construction du projet

Ce tuto présente un exemple avec le compilateur gcc qui est dans le paquet build-essentiel qui s'installe ainsi :

```
sudo apt-get install build-essential
```

Construire le projet

```
make
```

## L'exécutable

Le fichier exécutable se trouve dans le dossier bin. N'oubliez pas de préciser le numéro du fichier à charger lors de l'exécution en ligne de commande à l'aide du paramètre.

### Exemple


```
./prog 3
```


## Membres du groupe

* **Banugopan Kidnapillai** - *chef de groupe*
* **Mayouran Varathalingam** - *responsable technique*
* **Gilbert Delphin** - *responsable design*
* **Thomas Rakotovahiny** - *responsable communication*
* **Ludovic Le Creff** - *testeur*
