CFLAGS=-w -O2 -lSDL -lSDL_image -lSDL_ttf

all:
	gcc src/*.c -o bin/prog $(CFLAGS)

clean:
	rm -rf *.o

